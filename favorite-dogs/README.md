# Favorite Dogs

This project is a web application built using React that showcases a gallery of 6 random dog images from https://random.dog/woof.json. The user can choose one or more images to add to their favorites, which will be saved to the local storage. If the user doesn't like any of the six images displayed, they can use the refresh/next button to get 6 more images. This process can be repeated as many times as needed.

The application has two endpoints:

/ - The default endpoint that displays the gallery of 6 random dog images.
/favorites - An endpoint that displays all the favorite dog images that the user has saved.
Getting Started
To run the project locally, follow these steps:



## Running the application


```
cd existing_repo
git remote add origin https://gitlab.com/anirudhcenationuniverse/favorite-dogs-reactapp.git
git branch -M main
git push -uf origin main
```

Change into the project directory: cd favorite-dogs-reactapp
Install the project dependencies: npm install or yarn install
Start the development server: npm start or yarn start
Open your browser to http://localhost:3000 to view the application.


## Components

DogGallery
The DogGallery component displays a grid of DogCard components, representing the 6 random dog images. The user can use the refresh/next button to get 6 more images.

Favorites
The Favorites component displays a grid of DogCard components representing the user's favorite dog images. These images are fetched from the local storage.


## Unit Tests
Unit tests for the components have been written using Jest and Enzyme. To run the tests, run the following command:

npm test or yarn test