import React from 'react';
import { shallow } from 'enzyme';
import { DogGallery } from '../components/DogGallary';

describe('DogGallery component', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<DogGallery />);
  });

  it('renders without crashing', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders 6 dog cards', () => {
    expect(wrapper.find('.dog-card').length).toEqual(6);
  });

  it('handles adding dogs to favorites', () => {
    wrapper.find('.dog-card').first().simulate('click');
    expect(localStorage.getItem('favorites')).toContain('https://random.dog/woof.json');
  });
});
