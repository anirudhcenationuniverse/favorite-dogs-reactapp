import React from 'react';
import { shallow } from 'enzyme';
import { Favorites } from '../components/Favorites';

describe('Favorites component', () => {
  let wrapper;
  beforeEach(() => {
    localStorage.setItem('favorites', JSON.stringify(['https://random.dog/woof.json']));
    wrapper = shallow(<Favorites />);
  });

  it('renders without crashing', () => {
    expect(wrapper).toBeDefined();
  });

  it('renders 1 favorite dog', () => {
    expect(wrapper.find('.favorite-dog').length).toEqual(1);
  });

  it('handles removing dogs from favorites', () => {
    wrapper.find('.remove-favorite').simulate('click');
    expect(localStorage.getItem('favorites')).toEqual(null);
  });
});