
import './App.css';
import React from 'react';
import {   Route, Routes } from "react-router-dom";
import { DogGallery } from './components/DogGallary';
import { Favorites } from './components/Favorites';


function App() {
  return (
    <div className="App">
      <header className="App-header">
      <div>
      <Routes>
      <Route  path="/" element={<DogGallery/>} />
      <Route  path="/favorites" element={<Favorites/>} />
      </Routes>
    </div>
      </header>
    </div>
  );
}

export default App;
