

import React, { useState, useEffect } from 'react';

/**
 * DogGallery component displays a gallery of dog images fetched from random.dog API.
 *
 * @param {Array} dogs - List of dog images.
 * @param {function} refreshDogs - method to refreshs dogs with new random 6 dogs.
 * @param {function} addToFavorites - method to add dog to favorite.
 *
 * @returns {JSX.Element} A React component that displays the dog gallery.
 */


export const DogGallery = () => {
    const [dogs, setDogs] = useState([]);
    const [favorites, setFavorites] = useState(
      JSON.parse(localStorage.getItem('favorites')) || []
    );
   
    const fetchDogs = async () => {
      const response = await fetch("https://random.dog/woof.json");
      const data = await response.json();
      return data;
    };
  
    useEffect(() => {
      const fetchData = async () => {
        const dogs = [];
        for (let i = 0; i < 6; i++) {
          dogs.push(await fetchDogs());
        }
        console.log(dogs)
        setDogs(dogs.map(obj => obj.url));
      };
      fetchData();
    }, []);
  
    const refreshDogs = async () => {
      const dogs = [];
        for (let i = 0; i < 6; i++) {
          dogs.push(await fetchDogs());
        }
        setDogs(dogs.map(obj => obj.url));
      };
      
  
    const addToFavorites = url => {
      let favs = JSON.parse(localStorage.getItem('favorites'));
      
      // Add unique entry
      if(!favs.includes(url))
      { 
        setFavorites([...favorites, url]);
        localStorage.setItem('favorites', JSON.stringify([...favorites, url]));
      }
    };
  
    return (
      <div>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {dogs.map(dog => (
            <div class="container" key={dog} style={{ width: '33.33%', padding: '10px' }}>
              <img src={dog} alt="random dog" style={{ width: '100%' }} />
              <button class="btn" onClick={() => addToFavorites(dog)}>
              Add to favorites
            </button>
              </div>
          ))}
        </div>
        <button onClick={refreshDogs}>Refresh</button>
      </div>
    );
  };