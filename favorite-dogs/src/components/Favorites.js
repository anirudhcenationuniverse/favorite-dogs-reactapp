import React, { useState } from 'react';

/**
 * Favourite component displays the list of favorite dogs.
 *
 * @param {Array} favorites - List of favorite dogs.
 *
 * @returns {JSX.Element} A React component that displays the list of favorite dogs.
 */

export const Favorites = () => {
    const [favorites] = useState(
      JSON.parse(localStorage.getItem('favorites')) || []
    );
  
    return (
      <div style={{ display: 'flex', flexWrap: 'wrap' }}>
        {favorites.map(favorite => (
          <div key={favorite} style={{ width: '33.33%', padding: '10px' }}>
            <img src={favorite} alt="favorite dog" style={{ width: '100%' }} />
          </div>
        ))}
      </div>
    );
  };
  